"use client";

import {
    useFormField,
    Form,
    FormItem,
    FormLabel,
    FormControl,
    FormDescription,
    FormMessage,
    FormField,
} from "./components/ui/form";
import {Avatar, AvatarFallback, AvatarImage} from "./components/ui/avatar";
import InputField, {InnerFieldProps} from "./components/form/fields/input";
import BaseForm, {OnFormSubmitType} from "./components/form/base";
import ForgotPasswordForm, {ForgotPasswordFormProps} from "./components/form/forgotPassword";
import LoginForm, {LoginFormProps} from "./components/form/login";
import RegisterForm, {ZOD_PASSWORD_RULE, RegisterFormProps} from "./components/form/register";
import AuthCard, {AuthCardProps} from "./components/cards/auth";
import LoginCard, {LoginCardProps} from "./components/cards/login";
import RegisterCard, {RegisterCardProps} from "./components/cards/register";
import ForgotPasswordCard, {ForgotPasswordCardProps} from "./components/cards/forgotPassword";
import {AuthPage, AuthPageProps, AuthBasePage, AuthBasePageProps} from "./components/page/auth";
import AuthGuard, {AuthGuardProps} from "./components/auth/guard";
import AuthProvider, {AuthProviderProps} from "./components/auth/provider";
import {Input} from "./components/ui/input";
import CurrentPasswordField from "./components/form/fields/currentPassword";
import EmailField from "./components/form/fields/email";
import NewPasswordField from "./components/form/fields/newPassword";
import TextField from "./components/form/fields/text";
import UserAvatarDropdown, {UserAvatarDropdownProps} from "./components/header/dropdowns/userAvatar";
import ProtectedPage, {ProtectedPageProps} from "./components/page/protected";
import LoggedInHeader, {LoggedInHeaderProps} from "./components/header/loggedIn";
import {
    decodeToken,
    useAccessToken,
    useDecodedToken,
    useLoggedIn,
    useLogout,
    useRefreshToken,
    useSetToken,
    useUser,
    DecodedToken,
    DecodedTokenUser
} from "./hooks/useTokens";
import {
    DropdownMenu,
    DropdownMenuGroup,
    DropdownMenuContent,
    DropdownMenuItem,
    DropdownMenuCheckboxItem,
    DropdownMenuLabel,
    DropdownMenuPortal,
    DropdownMenuRadioGroup,
    DropdownMenuRadioItem,
    DropdownMenuSeparator,
    DropdownMenuShortcut,
    DropdownMenuSub,
    DropdownMenuSubContent,
    DropdownMenuSubTrigger,
    DropdownMenuTrigger
} from "./components/ui/dropdown-menu";
import LanguagePicker, {LanguagePickerProps} from "./components/languagePicker/index";
import LanguagePickerContents, {OnLanguageChangedType} from "./components/languagePicker/pickerContents";
import LanguagePickerMenuItem, {LanguagePickerMenuItemProps} from "./components/languagePicker/menuItem";
import LoggedOutHeader from "./components/header/loggedOut";
import CurrentFlagClient from "./components/currentFlag";
import UserAvatar, {UserAvatarProps} from "./components/userAvatar";

export {
    useFormField,
    Form,
    FormItem,
    FormLabel,
    FormControl,
    FormDescription,
    FormMessage,
    FormField,
    InputField,
    InnerFieldProps,
    BaseForm,
    OnFormSubmitType,
    ForgotPasswordForm,
    ForgotPasswordFormProps,
    LoginForm,
    LoginFormProps,
    RegisterForm,
    RegisterFormProps,
    ZOD_PASSWORD_RULE,
    AuthCard,
    AuthCardProps,
    LoginCard,
    LoginCardProps,
    RegisterCard,
    RegisterCardProps,
    ForgotPasswordCard,
    ForgotPasswordCardProps,
    AuthPageProps,
    AuthPage,
    AuthBasePageProps,
    AuthBasePage,
    AuthGuard,
    AuthGuardProps,
    Input,
    CurrentPasswordField,
    EmailField,
    NewPasswordField,
    TextField,
    UserAvatarDropdown,
    UserAvatarDropdownProps,
    ProtectedPage,
    ProtectedPageProps,
    LoggedInHeader,
    LoggedInHeaderProps,
    decodeToken,
    useAccessToken,
    useDecodedToken,
    useLoggedIn,
    useLogout,
    useRefreshToken,
    useSetToken,
    useUser,
    DecodedToken,
    DecodedTokenUser,
    DropdownMenu,
    DropdownMenuGroup,
    DropdownMenuContent,
    DropdownMenuItem,
    DropdownMenuCheckboxItem,
    DropdownMenuLabel,
    DropdownMenuPortal,
    DropdownMenuRadioGroup,
    DropdownMenuRadioItem,
    DropdownMenuSeparator,
    DropdownMenuShortcut,
    DropdownMenuSub,
    DropdownMenuSubContent,
    DropdownMenuSubTrigger,
    DropdownMenuTrigger,
    LanguagePicker,
    LanguagePickerProps,
    LanguagePickerContents,
    OnLanguageChangedType,
    LanguagePickerMenuItem,
    LanguagePickerMenuItemProps,
    LoggedOutHeader,
    CurrentFlagClient,
    Avatar,
    AvatarFallback,
    AvatarImage,
    UserAvatar,
    UserAvatarProps,
    AuthProvider,
    AuthProviderProps
}
