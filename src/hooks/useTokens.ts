"use client";

import {useContext} from "react";
import {AccessTokenContext, RefreshTokenContext, StaticContext} from "../components/auth/provider";
import {jwtDecode} from "jwt-decode";
import {LanguageType, TokenPair} from "@kudze/auth-guard-server"

export type DecodedTokenUser = {
    uuid: string,
    first_name: string,
    last_name: string,
    created_at: string,
    updated_at?: string,
    email: string,
    language: LanguageType
}

export type DecodedToken = {
    user: DecodedTokenUser,
    iat: number,
    exp: number
}

export function useAccessToken(): string | null {
    return useContext(AccessTokenContext) ?? null;
}

export function useRefreshToken(): string | null {
    return useContext(RefreshTokenContext) ?? null;
}

export function useDecodedToken(): DecodedToken | null {
    const token = useAccessToken();

    if (token === null)
        return null;

    return decodeToken(token);
}

export function useUser(): DecodedTokenUser | null {
    const token = useDecodedToken();

    if (token === null)
        return null;

    return token.user;
}

export function useLoggedIn(): boolean {
    return useAccessToken() !== null;
}

export function useLogout(): (() => void) | null {
    return useContext(StaticContext)?.logout ?? null;
}

export function useSetToken(): ((tokenPair: TokenPair) => void) | null {
    return useContext(StaticContext)?.setTokenPair ?? null;
}

export function decodeToken(token: string): DecodedToken {
    return jwtDecode(token);
}
