"use client";

import {useTranslation} from "react-i18next";
import {useLogout, useUser} from "../../../hooks/useTokens";
import {
    DropdownMenu,
    DropdownMenuContent, DropdownMenuItem,
    DropdownMenuLabel, DropdownMenuPortal,
    DropdownMenuSeparator, DropdownMenuSub, DropdownMenuSubContent, DropdownMenuSubTrigger,
    DropdownMenuTrigger
} from "../../ui/dropdown-menu";
import UserAvatar from "../../userAvatar";
import {ReactNode} from "react";
import {LanguageKeyType} from "@kudze/auth-guard-server"
import CurrentFlag from "../../currentFlag";
import LanguagePickerContents from "../../languagePicker/pickerContents";

export type UserAvatarDropdownProps = {
    children?: ReactNode,
    navigate: (url: string) => void,
    onLanguageChanged?: (locale: LanguageKeyType) => void
}

// @ts-ignore
let ACCOUNT_ROUTE = import.meta.env?.VITE_ACCOUNT_ROUTE;
if ((ACCOUNT_ROUTE ?? null) === null)
    ACCOUNT_ROUTE = process.env.NEXT_PUBLIC_ACCOUNT_ROUTE;
// @ts-ignore
let SESSION_ROUTE = import.meta.env?.VITE_SESSION_ROUTE;
if ((SESSION_ROUTE ?? null) === null)
    SESSION_ROUTE = process.env.NEXT_PUBLIC_SESSION_ROUTE;

export default function UserAvatarDropdown(
    {
        children,
        navigate,
        onLanguageChanged,
    }: UserAvatarDropdownProps
) {
    const {t} = useTranslation('auth');
    const user = useUser();
    const logout = useLogout();

    function _navigate(url: string) {
        if (url.startsWith("https")) window.location.replace(url);
        else navigate(url);
    }

    if (user === null)
        return null;

    return <div>
        <DropdownMenu>
            <DropdownMenuTrigger><UserAvatar user={user}/></DropdownMenuTrigger>
            <DropdownMenuContent className={"mt-3"}>
                <DropdownMenuLabel>{user.first_name} {user.last_name}</DropdownMenuLabel>
                <DropdownMenuSeparator/>
                <DropdownMenuItem
                    onClick={() => _navigate(ACCOUNT_ROUTE)}
                >
                    {t('user_dropdown.account_info')}
                </DropdownMenuItem>
                <DropdownMenuItem
                    onClick={() => _navigate(SESSION_ROUTE)}
                >
                    {t('user_dropdown.session_info')}
                </DropdownMenuItem>
                <DropdownMenuItem
                    onClick={() => logout ? logout() : null}
                >
                    {t('auth.logout')}
                </DropdownMenuItem>
                {children}
                <DropdownMenuSeparator/>
                <DropdownMenuSub>
                    <DropdownMenuSubTrigger>
                        <CurrentFlag/>
                    </DropdownMenuSubTrigger>
                    <DropdownMenuPortal>
                        <DropdownMenuSubContent>
                            <LanguagePickerContents onLanguageChanged={onLanguageChanged}/>
                        </DropdownMenuSubContent>
                    </DropdownMenuPortal>
                </DropdownMenuSub>
            </DropdownMenuContent>
        </DropdownMenu>
    </div>
}