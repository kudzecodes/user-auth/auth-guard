"use client";

import UserAvatarDropdown from "./dropdowns/userAvatar";
import {ReactNode} from "react";
import {useTranslation} from "react-i18next";
import {LanguageKeyType, Header} from "@kudze/auth-guard-server"


export type LoggedInHeaderProps = {
    children?: ReactNode,
    navigate: (url: string) => void,
    className?: string,
    onLanguageChanged?: (locale: LanguageKeyType) => void
}

export default function LoggedInHeader(
    {
        children,
        navigate,
        className,
        onLanguageChanged
    }: LoggedInHeaderProps
) {
    const {t} = useTranslation();

    return <Header t={t} className={className}>
        <UserAvatarDropdown navigate={navigate} onLanguageChanged={onLanguageChanged}>
            {children}
        </UserAvatarDropdown>
    </Header>
}