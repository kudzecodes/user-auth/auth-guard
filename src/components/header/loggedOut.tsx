"use client";

import {Header} from "@kudze/auth-guard-server"
import LanguagePicker from "../languagePicker";
import {useTranslation} from "react-i18next";

export default function LoggedOutHeader()
{
    const {t} = useTranslation();

    return <Header t={t}>
        <LanguagePicker/>
    </Header>
}