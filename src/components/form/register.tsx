import {useTranslation} from "react-i18next";
import {z} from "zod";
import {useForm} from "react-hook-form";
import {zodResolver} from "@hookform/resolvers/zod";
import {Button, LanguageType, register} from "@kudze/auth-guard-server"
import BaseForm from "./base";
import {FormField} from "../ui/form";
import EmailField from "./fields/email";
import NewPasswordField from "./fields/newPassword";
import TextField from "./fields/text";

export type RegisterFormProps = {
    onRegistered: () => void,
}

export const ZOD_PASSWORD_RULE = z.string().min(8).max(255);

export default function RegisterForm({onRegistered}: RegisterFormProps) {
    const {t, i18n} = useTranslation('auth');

    const formSchema = z.object({
        email: z.string().email().min(3).max(255),
        first_name: z.string().min(3).max(63),
        last_name: z.string().min(3).max(63),
        password: ZOD_PASSWORD_RULE,
        password_confirm: z.string(),
    }).superRefine(({password, password_confirm}, ctx) => {
        if (password !== password_confirm)
            ctx.addIssue({
                code: "custom",
                message: t('auth.error.password_confirm'),
                path: ['password_confirm']
            });
    })

    const form = useForm<z.infer<typeof formSchema>>({
        resolver: zodResolver(formSchema),
    });

    function onSubmit({email, first_name, last_name, password}: z.infer<typeof formSchema>): void {
        register({
            email: email,
            first_name: first_name,
            last_name: last_name,
            password: password,
            language: i18n.language as LanguageType
        })
            .then(onRegistered)
            .catch((e) => {
                let t = true;

                if (e.email !== undefined) {
                    form.setError('email', {message: e.email.join()})
                    t = false;
                }

                if (e.first_name !== undefined) {
                    form.setError('first_name', {message: e.first_name.join()})
                    t = false;
                }

                if (e.last_name !== undefined) {
                    form.setError('last_name', {message: e.last_name.join()})
                    t = false;
                }

                if (e.password !== undefined) {
                    form.setError('password', {message: e.password.join()})
                    t = false;
                }

                if (t) throw e;
            });
    }

    return <BaseForm form={form} onSubmit={onSubmit}>
        <FormField
            control={form.control}
            name="email"
            render={EmailField(
                form,
                t('auth.email'),
                t('auth.email_placeholder'),
            )}
        />

        <FormField
            control={form.control}
            name="first_name"
            render={TextField(
                form,
                t('auth.first_name'),
                t('auth.first_name_placeholder'),
                {autoComplete: "given-name"}
            )}
        />

        <FormField
            control={form.control}
            name="last_name"
            render={TextField(
                form,
                t('auth.last_name'),
                t('auth.last_name_placeholder'),
                {autoComplete: "family-name"}
            )}
        />

        <FormField
            control={form.control}
            name="password"
            render={NewPasswordField(
                form,
                t('auth.password'),
                t('auth.password_placeholder'),
            )}
        />

        <FormField
            control={form.control}
            name="password_confirm"
            render={NewPasswordField(
                form,
                t('auth.password_confirm'),
                t('auth.password_confirm_placeholder'),
            )}
        />

        <Button type="submit" className="w-full">{t('auth.register')}</Button>
    </BaseForm>;
}