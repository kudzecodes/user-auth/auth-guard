"use client";

import {FieldPath, FieldValues, UseFormReturn} from "react-hook-form";
import InputField from "./input";

export default function NewPasswordField<
    TFieldValues extends FieldValues = FieldValues,
    TName extends FieldPath<TFieldValues> = FieldPath<TFieldValues>
>(
    form: UseFormReturn<TFieldValues, TName>,
    label: string,
    placeholder: string,
    props = {},
    labelProps = {},
    messageProps = {},
) {
    return InputField(form, label, placeholder, 'password', {
        ...props,
        autoComplete: "new-password"
    }, labelProps, messageProps);
}