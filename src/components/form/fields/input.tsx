"use client";

import {FormControl, FormItem, FormLabel, FormMessage} from "../../ui/form";
import {Input} from "../../ui/input";
import { mapFormFieldToProps } from "@kudze/auth-guard-server"
import {HTMLInputTypeAttribute} from "react";
import {ControllerRenderProps, FieldPath, FieldValues, UseFormReturn} from "react-hook-form";

export type InnerFieldProps<
    TFieldValues extends FieldValues = FieldValues,
    TName extends FieldPath<TFieldValues> = FieldPath<TFieldValues>
> = {
    field: ControllerRenderProps<TFieldValues, TName>
};

export default function InputField<
    TFieldValues extends FieldValues = FieldValues,
    TName extends FieldPath<TFieldValues> = FieldPath<TFieldValues>
>(
    form: UseFormReturn<TFieldValues, TName>,
    label: string,
    placeholder: string | null,
    type: HTMLInputTypeAttribute,
    props = {},
    labelProps = {},
    messageProps = {}
) {
    if (placeholder !== null)
        props = {
            ...props,
            placeholder: placeholder
        };

    return function ({field}: InnerFieldProps<TFieldValues, TName>) {
        return <FormItem>
            <FormLabel {...labelProps}>{label}</FormLabel>
            <FormControl>
                <Input
                    type={type}
                    {...props}
                    {...mapFormFieldToProps(field, form, type)}
                />
            </FormControl>
            <FormMessage {...messageProps}/>
        </FormItem>
    }
}