import {z} from "zod";
import {zodResolver} from "@hookform/resolvers/zod";
import {FormField} from "../ui/form";
import {Button, forgotPasswordInitiate} from "@kudze/auth-guard-server"
import {useForm} from "react-hook-form";
import {useTranslation} from "react-i18next";
import BaseForm from "./base";
import EmailField from "./fields/email";

export type ForgotPasswordFormProps = {
    onFilled: (email: string) => void
}

export default function ForgotPasswordForm({onFilled}: ForgotPasswordFormProps) {
    const {t} = useTranslation('auth');

    const formSchema = z.object({
        email: z.string().email()
    }).required();

    const form = useForm<z.infer<typeof formSchema>>({
        resolver: zodResolver(formSchema),
    });

    function onSubmit({email}: z.infer<typeof formSchema>): void {
        forgotPasswordInitiate({
            email: email
        }).then(
            () => onFilled(email)
        ).catch((e) => {
            if (e.email !== undefined) {
                form.setError('email', {message: e.email.join()})
                return;
            }

            throw e;
        });
    }

    return <BaseForm form={form} onSubmit={onSubmit}>
        <FormField
            control={form.control}
            name="email"
            render={EmailField(
                form,
                t('auth.email'),
                t('auth.email_placeholder'),
            )}
        />

        <Button type="submit" className="w-full">{t('auth.continue')}</Button>
    </BaseForm>;
}