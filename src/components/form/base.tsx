"use client";

import {FieldValues, UseFormReturn} from "react-hook-form";
import {ReactNode} from "react";
import {cn} from "@kudze/auth-guard-server"
import {Form} from "../ui/form";

export type OnFormSubmitType = (form: any) => void;
export type BaseFormProps<TFieldValues extends FieldValues, TName> = {
    form: UseFormReturn<TFieldValues, TName>,
    onSubmit: OnFormSubmitType,
    className?: string,
    children: ReactNode
}

export default function BaseForm<TFieldValues extends FieldValues, TName>(
    {
        form,
        onSubmit,
        className,
        children
    }: BaseFormProps<TFieldValues, TName>
) {
    className = cn('space-y-6', className);

    return <Form {...form}>
        <form onSubmit={form.handleSubmit(onSubmit)} className={className}>
            {children}
        </form>
    </Form>
}