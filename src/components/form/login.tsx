import {z} from "zod";
import {zodResolver} from "@hookform/resolvers/zod";
import {FormField} from "../ui/form";
import {Button, login, TokenPair} from "@kudze/auth-guard-server"
import {useForm} from "react-hook-form";
import {useTranslation} from "react-i18next";
import BaseForm from "./base";
import EmailField from "./fields/email";
import CurrentPasswordField from "./fields/currentPassword";

export type LoginFormProps = {
    onAuth: (tokenPair: TokenPair) => void
}

export default function LoginForm({onAuth}: LoginFormProps) {
    const {t} = useTranslation('auth');

    const formSchema = z.object({
        email: z.string().email(),
        password: z.string()
    }).required();

    const form = useForm<z.infer<typeof formSchema>>({
        resolver: zodResolver(formSchema),
    });

    function onSubmit({email, password}: z.infer<typeof formSchema>): void {
        login({
            email: email,
            password: password
        })
            .then((token: TokenPair) => onAuth(token))
            .catch((e) => {
                let willThrow = true;

                if (e.credentials !== undefined) {
                    form.setError('email', {message: e.credentials.join()})
                    willThrow = false;
                }

                if (e.email !== undefined) {
                    form.setError('email', {message: e.email.join()})
                    willThrow = false;
                }

                if (willThrow)
                    throw e;
            })
    }

    return <BaseForm form={form} onSubmit={onSubmit}>
        <FormField
            control={form.control}
            name="email"
            render={EmailField(
                form,
                t('auth.email'),
                t('auth.email_placeholder'),
            )}
        />

        <FormField
            control={form.control}
            name="password"
            render={CurrentPasswordField(
                form,
                t('auth.password'),
                t('auth.password_placeholder')
            )}
        />

        <Button type="submit" className="w-full">{t('auth.login')}</Button>
    </BaseForm>;
}