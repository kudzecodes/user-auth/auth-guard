"use client";

import AuthCard, {AuthCardProps} from "../cards/auth";
import LoggedOutHeader from "../header/loggedOut";
import {ReactNode} from "react";
import { Container } from "@kudze/auth-guard-server"

export type AuthPageProps = AuthCardProps;

export function AuthPage(props: AuthCardProps) {
    return <AuthBasePage>
        <AuthCard {...props}/>
    </AuthBasePage>;
}

export type AuthBasePageProps = {
    children: ReactNode,
}

export function AuthBasePage({children}: AuthBasePageProps) {
    return <>
        <LoggedOutHeader/>
        <Container className={"my-5 md:mt-20 max-w-lg"}>
            {children}
        </Container>
    </>
}