"use client";

import {ReactNode} from "react";
import {Container, GlobalBreadcrumb, GlobalBreadcrumbNodeType, Toaster} from "@kudze/auth-guard-server"
import LoggedInHeader from "../header/loggedIn";
import {useTranslation} from "react-i18next";
import {OnLanguageChangedType} from "../languagePicker/pickerContents";

export type ProtectedPageProps = {
    children: ReactNode,
    breadcrumb?: GlobalBreadcrumbNodeType[]
    containerClass?: string,
    navigate: (url: string) => void
    onLanguageChanged?: OnLanguageChangedType
};

export default function ProtectedPage(
    {
        children,
        breadcrumb = [],
        containerClass = "my-5 md:mt-20 max-w-2xl",
        navigate,
        onLanguageChanged,
    }: ProtectedPageProps
) {
    const {t} = useTranslation();

    return <>
        <Toaster/>
        <LoggedInHeader navigate={navigate} onLanguageChanged={onLanguageChanged}/>
        <GlobalBreadcrumb
            nodes={[
                {
                    href: "/",
                    label: 'app.name'
                },
                ...breadcrumb,
            ]}
            t={t}
        />
        <Container className={containerClass}>
            {children}
        </Container>
    </>
}