"use client";

import {useState, useEffect, createContext, ReactNode} from "react";
import Cookies from "js-cookie";
import moment from "moment";
import {
    AccessTokenType,
    refreshToken,
    RefreshTokenType,
    TokenPair,
    logout as _logout,
    LanguageType
} from "@kudze/auth-guard-server"

import {decodeToken} from "../../hooks/useTokens";
import {toast} from "sonner";
import {AuthPage} from "../page/auth";
import {useTranslation} from "react-i18next";

export type AuthProviderProps = {
    children: ReactNode,
    isBlocking?: boolean,
    onAuthenticated?: () => void,
    onLanguageMismatch?: (lang: string) => void,
};

const TOKEN_PAIR_COOKIE = 'token';

//undefined for initializing.
//null for not logged in.
export type RealTokenPairType = TokenPair | null | undefined;

export type StaticContextType = {
    logout: () => void,
    setTokenPair: (tokenPair: TokenPair) => void,
};

export const StaticContext = createContext<StaticContextType | null>(null);
export const RefreshTokenContext = createContext<RefreshTokenType | null>(null);
export const AccessTokenContext = createContext<AccessTokenType | null>(null);

// @ts-ignore
let COOKIE_DOMAIN = import.meta.env?.VITE_COOKIE_DOMAIN;
if ((COOKIE_DOMAIN ?? null) === null)
    COOKIE_DOMAIN = process.env.NEXT_PUBLIC_COOKIE_DOMAIN;

const refreshBeforeSeconds = 10;

export default function AuthProvider(
    {
        children,
        isBlocking = false,
        onAuthenticated,
        onLanguageMismatch = undefined,
    }: AuthProviderProps
) {
    const {t, i18n} = useTranslation('auth');
    const [tokenPair, setTokenPair] = useState<RealTokenPairType>(undefined);

    //Loading tokens from a cookie.
    useEffect(() => {

        try {
            const cookieTokenPair = Cookies.get(TOKEN_PAIR_COOKIE);
            if (cookieTokenPair === undefined) {
                setTokenPair(null);
                return;
            }

            const decodedCookieTokenPair = JSON.parse(cookieTokenPair) as TokenPair | undefined;
            if (decodedCookieTokenPair === undefined) {
                setTokenPair(null);
                return;
            }

            //If access token has expired lets just show login screen.
            const decoded = decodeToken(decodedCookieTokenPair.access_token);
            const now = moment().unix();

            //If current access token is not expired we can just use that one.
            if (now <= decoded.exp) {
                setTokenPair(decodedCookieTokenPair);
                return;
            }

            //In this case we try to refresh token.
            refreshToken({refresh_token: decodedCookieTokenPair.refresh_token})
                .then(setTokenPair)
                .catch(() => setTokenPair(null));
        } catch (e) {
            setTokenPair(null);
        }

    }, []);

    //When ether token changes we update the cookie.
    useEffect(() => {
        //If loading then do nothing
        if (tokenPair === undefined) return;

        //If logged out then remove cookie.
        if (tokenPair === null) {
            Cookies.remove(TOKEN_PAIR_COOKIE, {
                domain: COOKIE_DOMAIN
            });
            return;
        }

        //If logged in the set cookie
        const options: Cookies.CookieAttributes = COOKIE_DOMAIN === 'localhost' ? {} : {
            sameSite: 'strict',
            domain: COOKIE_DOMAIN
        };

        Cookies.set(TOKEN_PAIR_COOKIE, JSON.stringify(tokenPair), options);

        if (onAuthenticated)
            onAuthenticated();

        //If there is alive token then we set up timeout to refresh it
        const decoded = decodeToken(tokenPair.access_token);
        const refreshAt = decoded.exp - refreshBeforeSeconds;
        const now = moment().unix();

        //If UI and User language differs report to prop if provided, change lang otherwise
        if (decoded.user.language !== i18n.language) {
            if (onLanguageMismatch)
                onLanguageMismatch(decoded.user.language);
            else
                i18n.changeLanguage(decoded.user.language);
        }

        const timeout = setTimeout(() => {
            refreshToken({refresh_token: tokenPair.refresh_token})
                .then((tokenPair) => {
                    toast.success(t('auth.access_token_refreshed'));
                    setTokenPair(tokenPair);
                })
                .catch(() => setTokenPair(null));
        }, Math.max(0, (refreshAt - now) * 1000));
        return () => clearTimeout(timeout);
    }, [tokenPair]);

    if (tokenPair === undefined)
        return null;

    if (tokenPair === null && isBlocking)
        return <AuthPage onLogin={(tokenPair: TokenPair) => setTokenPair(tokenPair)}/>

    function logout() {
        if (tokenPair === null || tokenPair === undefined)
            return;

        _logout({refresh_token: tokenPair.refresh_token})
            .catch(e => console.log("Failed to invalidate refresh token: " + e.toString()))
            .finally(() => setTokenPair(null));
    }

    return <StaticContext.Provider value={{
        logout: logout,
        setTokenPair: (tokenPair) => setTokenPair(tokenPair)
    }}>
        <RefreshTokenContext.Provider value={tokenPair?.refresh_token ?? null}>
            <AccessTokenContext.Provider value={tokenPair?.access_token ?? null}>
                {children}
            </AccessTokenContext.Provider>
        </RefreshTokenContext.Provider>
    </StaticContext.Provider>
}