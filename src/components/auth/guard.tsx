"use client";

import AuthProvider, {AuthProviderProps} from "./provider";

export type AuthGuardProps = Omit<AuthProviderProps, 'isBlocking'>;

export default function AuthGuard({children}: AuthGuardProps) {
    return <AuthProvider
        isBlocking={true}
    >
        {children}
    </AuthProvider>
}