"use client";

import {ContentType} from "./auth";
import LoginForm from "../form/login";
import {useTranslation} from "react-i18next";
import {
    Button,
    TokenPair,
    LoginAccountVerifiedAlert,
    LoginPasswordChangedAlert,
    Card,
    CardContent,
    CardDescription,
    CardHeader,
    CardTitle
} from "@kudze/auth-guard-server"

export type LoginCardProps = {
    onLogin: (tokenPair: TokenPair) => void,
    setContentType: (contentType: ContentType) => void,
}

export default function LoginCard(
    {
        onLogin, setContentType
    }: LoginCardProps
) {
    const {t} = useTranslation('auth');

    const params = new URLSearchParams(window.location.search);
    const registered = params.has('registered');
    const forgotPassword = params.has('forgotPassword');

    return <Card>
        <CardHeader>
            <CardTitle>{t('auth.login_heading')}</CardTitle>
            <CardDescription>{t('auth.login_subheading')}</CardDescription>
        </CardHeader>
        <CardContent>
            {registered ? <LoginAccountVerifiedAlert className={"mb-[24px]"} t={t}/> : null}
            {forgotPassword ? <LoginPasswordChangedAlert className={"mb-[24px]"} t={t}/> : null}
            <LoginForm onAuth={onLogin}/>
            <Button
                onClick={() => setContentType('forgotPassword')}
                className="w-full mt-5"
                variant="secondary"
            >
                {t('auth.forgot_password')}
            </Button>
            <Button
                onClick={() => setContentType('register')}
                className="w-full mt-5"
                variant="secondary"
            >
                {t('auth.no_account')}
            </Button>
        </CardContent>
    </Card>;
}