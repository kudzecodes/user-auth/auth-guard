"use client";

import {TokenPair} from "@kudze/auth-guard-server"
import {useState} from "react";
import LoginCard from "./login";
import RegisterCard from "./register";
import ForgotPasswordCard from "./forgotPassword";

export type AuthCardProps = {
    onLogin: (tokenPair: TokenPair) => void,
}

export type ContentType = "login" | "register" | "forgotPassword";

export default function AuthCard({onLogin}: AuthCardProps) {
    const [contentType, setContentType] = useState<ContentType>("login");

    switch (contentType) {
        case "login":
            return <LoginCard
                onLogin={onLogin}
                setContentType={setContentType}
            />
        case "register":
            return <RegisterCard
                setContentType={setContentType}
            />
        case "forgotPassword":
            return <ForgotPasswordCard
                setContentType={setContentType}
            />
    }
}