"use client";

import {ContentType} from "./auth";
import {Button, Card, CardContent, CardDescription, CardHeader, CardTitle} from "@kudze/auth-guard-server"

import RegisterForm from "../form/register";
import {useTranslation} from "react-i18next";

// @ts-ignore
let VERIFY_EMAIL_URL = import.meta.env?.VITE_AUTH_VERIFY_EMAIL_URL;
if ((VERIFY_EMAIL_URL ?? null) === null)
    VERIFY_EMAIL_URL = process.env.NEXT_PUBLIC_AUTH_VERIFY_EMAIL_URL;

export type RegisterCardProps = {
    setContentType: (contentType: ContentType) => void,
}

export default function RegisterCard({setContentType}: RegisterCardProps) {
    const {t} = useTranslation('auth');

    return <Card>
        <CardHeader>
            <CardTitle>{t('auth.register_heading')}</CardTitle>
            <CardDescription>{t('auth.register_subheading')}</CardDescription>
        </CardHeader>
        <CardContent>
            <RegisterForm
                onRegistered={() => window.location.replace(VERIFY_EMAIL_URL + "?registered=1")}
            />
            <Button
                onClick={() => setContentType('login')}
                className="w-full mt-5"
                variant="secondary"
            >
                {t('auth.have_account')}
            </Button>
        </CardContent>
    </Card>;
}