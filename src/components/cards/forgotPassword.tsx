"use client";

import {ContentType} from "./auth";
import {
    Card,
    CardContent,
    CardDescription,
    CardHeader,
    CardTitle,
    Button,
    ForgotPasswordFilledAlert
} from "@kudze/auth-guard-server"
import {useTranslation} from "react-i18next";
import {useState} from "react";
import ForgotPasswordForm from "../form/forgotPassword";

export type ForgotPasswordCardProps = {
    setContentType: (contentType: ContentType) => void,
}

export default function ForgotPasswordCard({setContentType}: ForgotPasswordCardProps) {
    const {t} = useTranslation('auth');

    const [filledEmail, setFilledEmail] = useState<string | undefined>(undefined);

    return <Card>
        <CardHeader>
            <CardTitle>{t('auth.forgot_password_heading')}</CardTitle>
            <CardDescription>{t('auth.forgot_password_subheading')}</CardDescription>
        </CardHeader>
        <CardContent>
            {
                filledEmail !== undefined
                    ? <ForgotPasswordFilledAlert email={filledEmail} t={t}/>
                    : <ForgotPasswordForm onFilled={setFilledEmail}/>
            }
            <Button
                onClick={() => setContentType('login')}
                className="w-full mt-5"
                variant="secondary"
            >
                {t('return')}
            </Button>
        </CardContent>
    </Card>;
}