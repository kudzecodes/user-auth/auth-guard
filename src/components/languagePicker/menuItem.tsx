"use client";

import {DropdownMenuItem} from "../ui/dropdown-menu";
import {Flag, LanguageKeyType, LANGUAGES} from "@kudze/auth-guard-server"

export type LanguagePickerMenuItemProps = {
    lang: LanguageKeyType,
    changeLanguage: (lang: LanguageKeyType) => void
};

export default function LanguagePickerMenuItem(
    {
        lang,
        changeLanguage
    }: LanguagePickerMenuItemProps
) {
    return <DropdownMenuItem onClick={() => changeLanguage(lang)}>
        <Flag iso2={lang} className={"mr-1"}/> {LANGUAGES[lang].name}
    </DropdownMenuItem>
}