"use client";

import {
    DropdownMenu,
    DropdownMenuContent,
    DropdownMenuTrigger
} from "../ui/dropdown-menu";
import LanguagePickerContents from "./pickerContents";
import {LanguageKeyType} from "@kudze/auth-guard-server";
import CurrentFlag from "../currentFlag";

export type LanguagePickerProps = {
    className?: string
    onLanguageChanged?: (locale: LanguageKeyType) => void
}

export default function LanguagePicker(
    {
        className = "",
        onLanguageChanged
    }: LanguagePickerProps
) {
    return <div className={className}>
        <DropdownMenu>
            <DropdownMenuTrigger>
                <CurrentFlag/>
            </DropdownMenuTrigger>
            <DropdownMenuContent className={"mt-5"}>
                <LanguagePickerContents onLanguageChanged={onLanguageChanged}/>
            </DropdownMenuContent>
        </DropdownMenu>
    </div>
}