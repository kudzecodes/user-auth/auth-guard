"use client";

import {useTranslation} from "react-i18next";
import {changeUserLanguage, LanguageKeyType, LANGUAGES} from "@kudze/auth-guard-server"
import {DropdownMenuLabel, DropdownMenuSeparator} from "../ui/dropdown-menu";
import LanguagePickerMenuItem from "./menuItem";
import {useAccessToken, useRefreshToken, useSetToken} from "../../hooks/useTokens";

export type OnLanguageChangedType = (locale: LanguageKeyType) => void;

export type LanguagePickerContentsProps = {
    onLanguageChanged?: OnLanguageChangedType
}

export default function LanguagePickerContents({onLanguageChanged}: LanguagePickerContentsProps) {
    const {t, i18n} = useTranslation('auth');
    const refreshToken = useRefreshToken();
    const setToken = useSetToken();

    function changeLanguage(locale: LanguageKeyType) {
        if (onLanguageChanged !== undefined)
            onLanguageChanged(locale);
        else if (refreshToken)
            changeUserLanguage({
                refresh_token: refreshToken,
                language: locale
            }).then(response => {
                if (setToken)
                    setToken({
                        refresh_token: refreshToken,
                        access_token: response.access_token
                    });
                i18n.changeLanguage(locale);
            });
        else i18n.changeLanguage(locale);
    }

    const availableLanguages = Object.keys(LANGUAGES);
    return <>
        <DropdownMenuLabel>{t('language_picker.pick')}</DropdownMenuLabel>
        <DropdownMenuSeparator/>
        {availableLanguages.map((lang, index) => <LanguagePickerMenuItem
            key={index} lang={lang as LanguageKeyType} changeLanguage={changeLanguage}
        />)}
    </>
}