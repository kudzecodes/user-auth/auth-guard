"use client";

import {useTranslation} from "react-i18next";
import {CurrentFlag} from "@kudze/auth-guard-server";

export default function CurrentFlagClient() {
    const {i18n} = useTranslation('auth');

    return <CurrentFlag i18n={i18n}/>
}