import {DecodedTokenUser} from "../hooks/useTokens";
import {Avatar, AvatarFallback} from "./ui/avatar";

export type UserAvatarProps = {
    user: DecodedTokenUser
}

export default function UserAvatar(
    {
        user: {
            first_name,
            last_name
        }
    }: UserAvatarProps) {
    const initials = `${first_name[0]}${last_name[0]}`;
    return <Avatar><AvatarFallback>{initials}</AvatarFallback></Avatar>;
}