import {defineConfig} from "tsup";

export default defineConfig({
    entry: ['src/index.ts'],
    format: ["esm"],
    dts: true, // Generate declaration file (.d.ts)
    splitting: false,
    sourcemap: true,
    clean: true,
    minify: true,
    external: ["react", "react-dom"],
});