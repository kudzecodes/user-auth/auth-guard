import i18n from "i18next";
import {initReactI18next} from "react-i18next";

import zodEnSnippet from 'zod-i18n-map/locales/en/zod.json';
import zodLtSnippet from 'zod-i18n-map/locales/lt/zod.json';
import authEnSnippet from '../snippets/en/translation.json';
import authLtSnippet from '../snippets/lt/translation.json';

import LanguageDetector from "i18next-browser-languagedetector";
import {z} from "zod";
import {zodI18nMap} from "zod-i18n-map";

i18n.use(initReactI18next).use(LanguageDetector).init({
    resources: {
        en: {
            translation: {"app.name": "Kudze's Auth Guard"},
            zod: zodEnSnippet,
            auth: authEnSnippet
        },
        lt: {
            translation: {"app.name": "Kudze's Auth Guard"},
            zod: zodLtSnippet,
            auth: authLtSnippet
        }
    },
    detection: {
        order: ['querystring', 'cookie'],
        caches: ['cookie'],
        lookupQuerystring: 'lang'
    },
    interpolation: {
        escapeValue: false,
    },
    fallbackLng: "en",
});
z.setErrorMap(zodI18nMap);