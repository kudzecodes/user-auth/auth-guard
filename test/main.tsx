import React from 'react'
import ReactDOM from 'react-dom/client'
import "./globals.css"

import "./i18n";

import AuthGuard from "../src/components/auth/guard";

import {BrowserRouter, Route, Routes} from "react-router-dom";

function TestPage() {
    return <AuthGuard>
        You logged in!
    </AuthGuard>;
}

ReactDOM.createRoot(document.getElementById('root')!).render(
    <React.StrictMode>
        <BrowserRouter>
            <Routes>
                <Route element={<TestPage/>} path={"/"}/>
            </Routes>
        </BrowserRouter>
    </React.StrictMode>,
)
