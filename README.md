# Auth Guard

## Description

This is a small UI that all of my services uses for authentications.

## Environment variables

### In case used from Vite

* `VITE_AUTH_API_URL` - URL of authenticator (default: `https://auth.kudze.lt/api`).
* `VITE_AUTH_VERIFY_EMAIL_URL` - URL of verify email page (default: `https://auth.kudze.lt/verify/`).
* `VITE_COOKIE_DOMAIN` - Cookie domain for authentication (default: `https://auth.kudze.lt/api`).
* `VITE_ACCOUNT_ROUTE` - URL of account page (default: `https://auth.kudze.lt/account`).
* `VITE_SESSION_ROUTE` - URL of session route (default: `https://auth.kudze.lt/session`).

### In case used from NextJs

* `NEXT_PUBLIC_AUTH_API_URL` - URL of authenticator (default: `https://auth.kudze.lt/api`).
* `NEXT_PUBLIC_AUTH_VERIFY_EMAIL_URL` - URL of verify email page (default: `https://auth.kudze.lt/verify/`).
* `NEXT_PUBLIC_COOKIE_DOMAIN` - Cookie domain for authentication (default: `https://auth.kudze.lt/api`).
* `NEXT_PUBLIC_ACCOUNT_ROUTE` - URL of account page (default: `https://auth.kudze.lt/account`).
* `NEXT_PUBLIC_SESSION_ROUTE` - URL of session route (default: `https://auth.kudze.lt/session`).